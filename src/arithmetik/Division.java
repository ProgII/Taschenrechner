package arithmetik;

public class Division implements BinaryOperator {

	private int leftValue;
	private int rightValue;
	
	public Division(int leftValue, int rightValue){
		assert rightValue == 0 : "Divisor darf nicht gleich 0 sein!";
		this.leftValue = leftValue;
		this.rightValue = rightValue;
	}
	
	public int getLeftValue() {
		return leftValue;
	}

	public int getRightValue() {
		return rightValue;
	}
	
	@Override
	public int calculate() {
		return leftValue / rightValue;
	}
	
	@Override
	public void prettyPrint(){
		System.out.println("Das Ergebnis der Formel '" + this.leftValue + " / " + this.rightValue + "' ist:" );
	}

}
