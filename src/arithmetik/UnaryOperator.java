package arithmetik;

/**
 * Das Interface UnaryOperator gibt das Grundgerüst für einen einstelligen Operator
 * 
 * @author ProgII Totorenteam
 *
 */

public interface UnaryOperator {
	public int getValue();
	
	
	/**
	 * Diese Methode gibt das Ergebnis der jeweiligen Operation zurück
	 * @return
	 */
	public int calculate();
	
	/**
	 * Eine Ausgabe der berechneten Formel.
	 */
	public void prettyPrint();
}
