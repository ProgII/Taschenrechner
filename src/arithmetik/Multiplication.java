package arithmetik;

public class Multiplication implements BinaryOperator {

	private int leftValue;
	private int rightValue;
	
	public Multiplication(int leftValue, int rightValue){
		this.leftValue = leftValue;
		this.rightValue = rightValue;
	}
	
	public int getLeftValue() {
		return leftValue;
	}

	public int getRightValue() {
		return rightValue;
	}
	
	@Override
	public int calculate() {
		return leftValue * rightValue;
	}
	
	@Override
	public void prettyPrint(){
		System.out.println("Das Ergebnis der Formel '" + this.leftValue + " * " + this.rightValue + "' ist:" );
	}

}
