package arithmetik;

public class Faculty implements UnaryOperator {

private int value;
	
	public Faculty(int value){
		assert value < 0 : "Werte dürfen nicht kleiner 0 sein!";
		this.value = value;
	}
	
	public int getValue() {
		return value;
	}
	
	@Override
	public int calculate() {
		int res = 1;
		for(int i = 1; i < value; i++){
			res *= i;
		}
		return res;
	}
	
	@Override
	public void prettyPrint(){
		System.out.println("Das Ergebnis der Formel '" + this.value + "!' ist:" );
	}

}
