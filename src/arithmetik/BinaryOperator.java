package arithmetik;

/**
 * Das Interface BinaryOperator gibt das Grundgerüst für zweistellige Operatoren.
 * 
 * @author ProgII Tutorenteam
 *
 */
public interface BinaryOperator {
	
	public int getLeftValue();
	public int getRightValue();
	
	
	/**
	 * Diese Methode gibt das Ergebnis der jeweiligen Operation zurück
	 * @return
	 */
	public int calculate();
	
	/**
	 * Die Ausgabe einer berechneten Formel.
	 */
	public void prettyPrint();
	
}
