package arithmetik;

/**
 * Die Klasse Addition implementiert 
 * die Subtraktion zweier Werte aufgrund der Schnittstelle 'BinaryOperator'
 * 
 * @author ProgII Tutorenteam
 *
 */

public class Subtraction implements BinaryOperator {

	private int leftValue;
	private int rightValue;
	
	public Subtraction(int leftValue, int rightValue){
		this.leftValue = leftValue;
		this.rightValue = rightValue;
	}
	
	public int getLeftValue() {
		return leftValue;
	}

	public int getRightValue() {
		return rightValue;
	}
	
	@Override
	public int calculate() {
		return leftValue - rightValue;
	}
	
	
	/**
	 * Eine Ausgabe der berechneten Formel
	 */
	@Override
	public void prettyPrint(){
		System.out.println("Das Ergebnis der Formel '" + this.leftValue + " - " + this.rightValue + "' ist:" );
	}
}
