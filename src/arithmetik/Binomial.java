package arithmetik;

public class Binomial implements BinaryOperator {

	private int leftValue;
	private int rightValue;
	
	public Binomial(int leftValue, int rightValue){
		assert leftValue >= 0 : "Werte dürfen nicht kleiner 0 sein!";
		assert rightValue >= 0 : "Werte dürfen nicht kleiner 0 sein!";
		this.leftValue = leftValue;
		this.rightValue = rightValue;
	}
	
	public int getLeftValue() {
		return leftValue;
	}

	public int getRightValue() {
		return rightValue;
	}
	
	@Override
	public int calculate() {
		
		int leftFac = new Faculty(leftValue).calculate();
		int rightFac = new Faculty(rightValue).calculate();
		
		int leftRight = new Subtraction(leftValue, rightValue).calculate();
		
		int leftRightFac = new Faculty(leftRight).calculate();
		
		int denominator = new Multiplication(rightFac, leftRightFac).calculate();
		
		int res = new Division(leftFac, denominator).calculate();
		
		return res;
	}

	@Override
	public void prettyPrint(){
		System.out.println("Das Ergebnis der Formel '" + this.leftValue + " über " + this.rightValue + "' ist:" );
	}
}
