package arithmetik;

public class Negation implements UnaryOperator {

	private int value;
	
	public Negation(int value){
		this.value = value;
	}
	
	public int getValue() {
		return value;
	}
	
	@Override
	public int calculate() {
		return --value;
	}
	
	@Override
	public void prettyPrint(){
		System.out.println("Das Ergebnis der Formel '~" + this.value + "' ist:" );
	}

}
