package arithmetik;

/**
 * Die Klasse Addition implementiert 
 * die Addition zweier Werte aufgrund der Schnittstelle 'BinaryOperator'
 * 
 * @author ProgII Tutorenteam
 *
 */

public class Addition implements BinaryOperator {
	private int leftValue;
	private int rightValue;
	
	public Addition(int leftValue, int rightValue){
		this.leftValue = leftValue;
		this.rightValue = rightValue;
	}
	
	public int getLeftValue() {
		return leftValue;
	}

	public int getRightValue() {
		return rightValue;
	}
	
	@Override
	public int calculate() {
		return this.leftValue + this.rightValue;
	}
	
	
	/**
	 * Eine Ausgabe der errechneten Formel
	 */
	@Override
	public void prettyPrint(){
		System.out.println("Das Ergebnis der Formel '" + this.leftValue + " + " + this.rightValue + "' ist:" );
	}

}
