package main;

import arithmetik.*;

public class Main {
	public static void main(String[] args){
		int res = 0;
		int upperBound = 1000000;
		if ((args.length < 2) || (args.length > 3)) printHelp();
		int leftValue = Integer.parseInt(args[1]);
		int rightValue = upperBound + 1;
		assert (leftValue <= upperBound && leftValue >= -upperBound) : "LeftValue out of range!";
		if (args.length == 3){
			rightValue = Integer.parseInt(args[2]);
			assert (rightValue <= upperBound && rightValue >= -upperBound): "RightValue out of range!";
		}
		switch (args[0]) {
		case "+":
			assert args.length == 3 : "Nicht die richtige Zahl an Argumenten!";
			Addition add = new Addition(leftValue, rightValue);
			res = add.calculate();
			add.prettyPrint();
			break;
		case "-":
			assert args.length == 3 : "Nicht die richtige Zahl an Argumenten!";
			Subtraction sub = new Subtraction(leftValue, rightValue);
			res = sub.calculate();
			sub.prettyPrint();
			break;
		case "*":
			assert args.length == 3 : "Nicht die richtige Zahl an Argumenten!";
			Multiplication mult = new Multiplication(leftValue, rightValue);
			res = mult.calculate();
			mult.prettyPrint();
			break;
		case "/":
			assert args.length == 3 : "Nicht die richtige Zahl an Argumenten!";
			Division div = new Division(leftValue, rightValue);
			res = div.calculate();
			div.prettyPrint();
			break;
		case "()":
			assert args.length == 3 : "Nicht die richtige Zahl an Argumenten!";
			Binomial bin = new Binomial(leftValue, rightValue);
			res = bin.calculate();
			bin.prettyPrint();
			break;
		case "!":
			assert args.length == 2 : "Nicht die richtige Zahl an Argumenten!";
			Faculty fac = new Faculty(leftValue);
			res = fac.calculate();
			fac.prettyPrint();
			break;
		case "~":
			assert args.length == 2 : "Nicht die richtige Zahl an Argumenten!";
			Negation neg = new Negation(leftValue);
			res = neg.calculate();
			neg.prettyPrint();
			break;
		default:
			printHelp();
			break;
		}
		
		System.out.println(res);
	}
	private static void printHelp(){
		System.out.println("Geben sie eine Formel der Form 'op a b' oder 'op a' an, wobei a und b vom Typ int sind.");
		System.out.println("Mögliche Operatoren sind:");
		System.out.println("'+' -  Addiert zwei Integer");
		System.out.println("'-' -  Suptrahiert zwei Integer");
		System.out.println("'*' -  Multipliziert zwei Integer");
		System.out.println("'/' -  Dividiert zwei Integer");
		System.out.println("'()' - Berechnet a über b");
		System.out.println("'!' -  Berechnet die Fakultät von a");
		System.out.println("'~' -  Negiert a");
		System.exit(-1);
	}

}
