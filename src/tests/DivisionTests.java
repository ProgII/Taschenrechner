package tests;

import arithmetik.Division;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class DivisionTests {

    @Test
    public void divisionTest0() {
        Division div = new Division(12, 4);
        assertTrue(div.calculate() == 3);
    }

    @Test(expected = AssertionError.class)
    public void invalidDivisionTest() {
        Division div = new Division(42, 0);
    }

    // TODO: Fügen Sie weitere Tests hinzu!

}
